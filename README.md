# Introduction
Test task Guardtime for Back-End Engineer position

# Requirements
Java 11
+ [OpenJDK](http://openjdk.java.net/)

Gradle 6+
+ [Gradle](https://gradle.org/install/)

# Features
+ create containers from existing files in zip format
+ open created containers
+ sign containers
+ remove signatures from containers

Containers have following structure
```text
    /datafile1.txt
    /datafile2.pdf
    /datafile3.png 
    /META-INF/manifest1.tlv 
    /META-INF/signature1.ksi 
    /META-INF/manifest2.tlv 
    /META-INF/signature2.ksi
```

Manifests in containers are in tlv format and have following structure
```text
    datafile
        uri= datafile1.txt 
        hashalgorithm=SHA256 
        hash=...
        
    datafile
        uri= datafile2.pdf 
        hashalgorithm=SHA512 
        hash=...
        
    signature-uri=/METAINF/signature1.ksi
```

# Usage
Access to the KSI platform is required, for trial go to [https://guardtime.com/blockchain-developers](https://guardtime.com/blockchain-developers).

### Creating container
Implementation of _Container Service API_ is in class _ZipContainerServiceImpl_ that requires ksi 
credentials for creating an instance

```java
ZipContainerServiceImpl containerService = new ZipContainerServiceImpl(aggregatorUrl, loginId, loginKey)
```

To create a new container, use method _createCOntainer_
```java
createContainer(File container)
```

To open a container, use method _openContainer_
```java
openContainer(File container)
```

When container is created or opened, service api is in open state. To add files to container, 
use method _addFile_ (allows adding all files with different hash algorithms) or _addFiles_
that will use the same algorithm for all files
```java
addFile(File file, HashAlgorithm algorithm)
        
addFiles(List<File> files, HashAlgorithm algorithm)
```

To sign a container and add corresponding manifest, use method _addSignature_
```java
addSignature()
```

To remove signature from an opened container, use method _removeSignature_
```java
removeSignature(String signatureUri)
```

Finally use method _closeContainer_ to write changes to file
```java
closeContainer()
```


A simple example how create a container and sign it:
```java
        ZipContainerServiceImpl containerService = new ZipContainerServiceImpl(aggregatorUrl, loginId, loginKey);
        File container = new File("/dir/to/container.zip");
        containerService.createContainer(container);
        File file = new File("/dir/to/file.txt");
        containerService.addFile(file, HashAlgorithm.SHA2_256);
        containerService.addSignature();
        containerService.closeContainer();
```

A editing container - sign, add file, sign again:
```java
        ZipContainerServiceImpl containerService = new ZipContainerServiceImpl(aggregatorUrl, loginId, loginKey);
        ....load container file
        containerService.openContainer(loadedContainerFile);
        containerService.addSignature();
        File file = new File("/dir/to/newAddedFile.txt");
        containerService.addFile(file, HashAlgorithm.SHA2_512);
        containerService.addSignature();
        containerService.closeContainer();
```

A editing container - removing a signature:
```java
        ZipContainerServiceImpl containerService = new ZipContainerServiceImpl(aggregatorUrl, loginId, loginKey);
        ....load container file
        containerService.openContainer(loadedContainerFile);
        containerService.removeSignature("/META-INF/signature1.ksi");
        containerService.closeContainer();
```

# Developing
### Running checkstyle and test
```shell
./gradlew clean build -Dksi.login.key=[login-key] -Dksi.login.id=[login-id] -Dksi.aggregator.url=[signing-service-url]
```
or for only running tests use command
```shell
./gradlew test -Dksi.login.key=[login-key] -Dksi.login.id=[login-id] -Dksi.aggregator.url=[signing-service-url]
```