package com.guardtime.guardtimetask.common.exception;

@SuppressWarnings({ "checkstyle:FinalParameters" })
public class ContainerServiceException extends RuntimeException {
    private static final long serialVersionUID = 4076119488371982779L;

    public ContainerServiceException() {
        super();
    }

    public ContainerServiceException(final String message) {
        super(message);
    }

    public ContainerServiceException(final String message, Throwable cause) {
        super(message, cause);
    }

    public ContainerServiceException(Throwable cause) {
        super(cause);
    }
}
