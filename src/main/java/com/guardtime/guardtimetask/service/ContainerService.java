package com.guardtime.guardtimetask.service;

import com.guardtime.ksi.exceptions.KSIException;
import com.guardtime.ksi.hashing.HashAlgorithm;
import com.guardtime.ksi.tlv.TLVParserException;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface ContainerService {

    void createContainer(File container);

    void openContainer(File container);

    void closeContainer();

    void addFile(File file, HashAlgorithm algorithm) throws IOException, TLVParserException;

    void addFiles(List<File> files, HashAlgorithm algorithm) throws IOException, TLVParserException;

    void addSignature() throws KSIException, IOException;

    void removeSignature(String signatureUri);

}
