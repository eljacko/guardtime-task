package com.guardtime.guardtimetask.service;

import com.guardtime.guardtimetask.common.exception.ContainerServiceException;
import com.guardtime.guardtimetask.service.manifest.ManifestService;
import com.guardtime.guardtimetask.service.manifest.ManifestServiceImpl;
import com.guardtime.guardtimetask.service.manifest.model.Datafile;
import com.guardtime.guardtimetask.service.manifest.model.Manifest;
import com.guardtime.guardtimetask.service.packaging.PackagingService;
import com.guardtime.guardtimetask.service.packaging.ZipPackagingServiceImpl;
import com.guardtime.guardtimetask.service.signing.SigningService;
import com.guardtime.guardtimetask.service.signing.KsiSigningServiceImpl;
import com.guardtime.ksi.exceptions.KSIException;
import com.guardtime.ksi.hashing.HashAlgorithm;
import com.guardtime.ksi.tlv.TLVElement;
import com.guardtime.ksi.tlv.TLVParserException;
import com.guardtime.ksi.unisignature.KSISignature;
import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipContainerServiceImpl implements ContainerService {
    private static Logger log = LoggerFactory.getLogger(ZipContainerServiceImpl.class);
    private static final String META_INFO_FOLDER = "/META-INF";

    private final SigningService signingService;
    private final ManifestService manifestService;
    private final PackagingService packagingService;

    private int metaInfoFileNumber;
    @Getter
    private List<String> loadedFiles = new ArrayList<String>();
    @Getter
    private List<Datafile> datafiles = new ArrayList<Datafile>();
    @Getter
    private Map<String, byte[]> addedMetadata = new HashMap<String, byte[]>();
    private ZipOutputStream zipOutputStream;
    private File openedContainer = null;
    private File temporaryContainer = null;

    public ZipContainerServiceImpl(final String aggregatorUrl, final String loginId,
                                   final String loginKey) {
        signingService = new KsiSigningServiceImpl(aggregatorUrl, loginId, loginKey);
        manifestService = new ManifestServiceImpl();
        packagingService = new ZipPackagingServiceImpl();
    }

    /**
     * Creates new container and initiates stream
     *
     * @param containerFile destination file for container
     */
    @Override
    public void createContainer(final File containerFile) {
        log.debug("Creating container");
        if (this.zipOutputStream != null) {
            log.warn("Attempt to create container while previous open");
            throw new ContainerServiceException("Container open, close to create new!");
        }
        this.metaInfoFileNumber = 1;
        try {
            signingService.setUpKsi();
        } catch (KSIException e) {
            log.error("Setting up KSI signing service failed!");
            throw new ContainerServiceException("Setting up KSI signing service failed!", e);
        }
        try {
            this.zipOutputStream = new ZipOutputStream(new FileOutputStream(containerFile));
        } catch (FileNotFoundException e) {
            log.error("Container file not found!");
            throw new ContainerServiceException("Container file not found!", e);
        }
    }

    /**
     * Open container modifying, also initiates streams and loads meta info
     */
    @Override
    public void openContainer(final File containerFile) {
        log.debug("Opening container");
        if (this.zipOutputStream != null) {
            log.warn("Attempt to create container while previous open");
            throw new ContainerServiceException("Container open, close to create new!");
        }
        this.metaInfoFileNumber = 1;
        try {
            this.temporaryContainer = File.createTempFile("temp_", ".zip");
            this.zipOutputStream = new ZipOutputStream(new FileOutputStream(temporaryContainer));
            this.openedContainer = containerFile;
            ZipInputStream zis = new ZipInputStream(new FileInputStream(openedContainer));
            ZipEntry entry;
            while ((entry = zis.getNextEntry()) != null) {
                loadedFiles.add(entry.getName());
                if (entry.getName().startsWith("/META-INF/manifest")) {
                    byte[] manifestBytes = packagingService.extractEntry(zis);
                    Manifest manifest = new Manifest(TLVElement.create(manifestBytes));
                    for (Datafile datafile : manifest.getDatafiles()) {
                        if (!containsDatafile(datafiles, datafile.getUri())) {
                            datafiles.add(datafile);
                        }
                    }

                    String number = entry.getName()
                            .replace("/META-INF/manifest", "")
                            .replace(".tlv", "");
                    if (metaInfoFileNumber <= Integer.parseInt(number)) {
                        metaInfoFileNumber = Integer.parseInt(number) + 1;
                    }
                }
            }
            this.zipOutputStream = new ZipOutputStream(new FileOutputStream(temporaryContainer));
        } catch (IOException e) {
            log.error("Opening container failed, IOException");
            throw new ContainerServiceException("Opening container failed, IOException!");
        } catch (TLVParserException e) {
            log.error("Opening container failed, TLVParserException");
            throw new ContainerServiceException("Opening container failed, TLVParserException!");
        }
    }

    /**
     * Finishes container, writes it to file and closes streams
     */
    @Override
    public void closeContainer() {
        validateContainerActionsAllowed();
        log.debug("Closing down container");
        try {
            for (String uri : addedMetadata.keySet()) {
                packagingService.addFile(zipOutputStream,
                        new ByteArrayInputStream(addedMetadata.get(uri)), uri);
            }
            if (openedContainer != null) {
                ZipInputStream source =
                        new ZipInputStream(new FileInputStream(openedContainer));
                packagingService.transferEntries(source, zipOutputStream, loadedFiles);
            }

            zipOutputStream.close();
            zipOutputStream = null;
            datafiles.clear();
            addedMetadata.clear();

            if (openedContainer != null) {
                Path temp = temporaryContainer.toPath();
                Path opened = openedContainer.toPath();
                Files.move(temp, opened, StandardCopyOption.REPLACE_EXISTING);
            }
            openedContainer = null;
            temporaryContainer = null;
            loadedFiles.clear();
        } catch (IOException e) {
            log.error("Error saving container");
            throw new ContainerServiceException("Error saving container: ", e);
        } finally {
            signingService.tearDownKsi();
        }
    }

    /**
     * Adds a file to container
     *
     * @param file a file to be added
     * @param algorithm hash algorithm for datafile
     * @throws ContainerServiceException
     */
    @Override
    public void addFile(final File file, final HashAlgorithm algorithm)
            throws ContainerServiceException {
        validateContainerActionsAllowed();
        log.debug(String.format("Adding file: %s to container, HashAlgorithm: %s",
                file.getName(), algorithm.getName()));
        try {
            Datafile datafile = manifestService.createDatafile(algorithm, file);
            datafiles.add(datafile);
            FileInputStream inputStream = new FileInputStream(file);
            packagingService.addFile(zipOutputStream, inputStream, file.getName());
        } catch (IOException | TLVParserException e) {
            log.error("Adding file failed");
            throw new ContainerServiceException("Error adding file to container: ", e);
        }
    }

    /**
     * Adds a list of fies to a open container
     *
     * @param files list of files
     * @param algorithm hash algorithm for datafile
     */
    @Override
    public void addFiles(final List<File> files, final HashAlgorithm algorithm) {
        log.debug(String.format("Adding %n files to conainer", files.size()));
        validateContainerActionsAllowed();
        for (File file : files) {
            addFile(file, algorithm);
        }
    }

    /**
     * Creates a new manifest and adds signature
     *
     * @throws ContainerServiceException
     */
    @Override
    public void addSignature() throws ContainerServiceException {
        validateContainerActionsAllowed();
        log.debug("Adding signature to container");
        String signatureUri = META_INFO_FOLDER + "/signature" + metaInfoFileNumber + ".ksi";
        String manifestUri = META_INFO_FOLDER + "/manifest" + metaInfoFileNumber + ".tlv";
        metaInfoFileNumber++;

        try {
            Manifest manifest = manifestService.createManifest(signatureUri);
            for (Datafile df : datafiles) {
                manifest.addDatafile(df);
            }
            ByteArrayOutputStream manifestOutputStream = new ByteArrayOutputStream();
            manifest.writeTo(manifestOutputStream);
            final byte[] manifestBytes = manifestOutputStream.toByteArray();
            KSISignature signature = signingService.sign(manifestBytes);
            ByteArrayOutputStream signatureOutputStream = new ByteArrayOutputStream();
            signature.writeTo(signatureOutputStream);
            final byte[] signatureBytes = signatureOutputStream.toByteArray();
            addedMetadata.put(manifestUri, manifestBytes);
            addedMetadata.put(signatureUri, signatureBytes);
        } catch (KSIException e) {
            log.error("Adding signature failed");
            throw new ContainerServiceException("Adding signature failed", e);
        }
    }

    /**
     * Removes signature from container and deletes associated manifest
     *
     * @param signatureUri
     */
    @Override
    public void removeSignature(final String signatureUri) {
        log.debug("Removing signature: " + signatureUri);
        validateContainerActionsAllowed();
        String manifestUri = signatureUri
                .replace("signature", "manifest")
                .replace("ksi", "tlv");
        loadedFiles.remove(signatureUri);
        loadedFiles.remove(manifestUri);
        addedMetadata.remove(signatureUri);
        addedMetadata.remove(manifestUri);
    }

    private void validateContainerActionsAllowed() {
        if (this.zipOutputStream == null) {
            log.warn("Action not allowed, container not open");
            throw new ContainerServiceException("Action not allowed, container not open");
        }
    }

    private boolean containsDatafile(final List<Datafile> list, final String uri) {
        return list.stream().map(Datafile::getUri).filter(uri::equals).findFirst().isPresent();
    }

}
