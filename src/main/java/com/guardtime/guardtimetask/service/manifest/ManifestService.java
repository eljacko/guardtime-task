package com.guardtime.guardtimetask.service.manifest;

import com.guardtime.guardtimetask.service.manifest.model.Datafile;
import com.guardtime.guardtimetask.service.manifest.model.Manifest;
import com.guardtime.ksi.hashing.HashAlgorithm;
import com.guardtime.ksi.tlv.TLVParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

public interface ManifestService {

    Manifest createManifest(String signatureUri) throws TLVParserException;

    Manifest createManifest(String signatureUri, HashAlgorithm hashAlgorithm,
                            InputStream inputStream, String fileName) throws TLVParserException;

    Datafile createDatafile(HashAlgorithm hashAlgorithm, InputStream inputStream, String fileName)
            throws TLVParserException;

    Datafile createDatafile(HashAlgorithm hashAlgorithm, File file)
            throws TLVParserException, FileNotFoundException;

}
