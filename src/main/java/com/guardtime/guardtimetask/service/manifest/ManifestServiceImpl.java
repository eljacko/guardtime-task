package com.guardtime.guardtimetask.service.manifest;

import com.guardtime.guardtimetask.service.manifest.model.Datafile;
import com.guardtime.guardtimetask.service.manifest.model.Manifest;
import com.guardtime.ksi.hashing.DataHash;
import com.guardtime.ksi.hashing.DataHasher;
import com.guardtime.ksi.hashing.HashAlgorithm;
import com.guardtime.ksi.hashing.HashException;
import com.guardtime.ksi.tlv.TLVParserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Service for creating container meta-info: manifests and datafiles
 */
public class ManifestServiceImpl implements ManifestService {
    private static Logger log = LoggerFactory.getLogger(ManifestServiceImpl.class);

    /**
     * Create manifest with one datafile entry
     *
     * @param signatureUri signature uri in the container
     * @param hashAlgorithm Hash algorithm to be used
     * @param inputStream file input stream
     * @param fileName file name
     * @return Manifest
     * @throws TLVParserException
     */
    @Override
    public Manifest createManifest(final String signatureUri, final HashAlgorithm hashAlgorithm,
                                   final InputStream inputStream, final String fileName)
            throws TLVParserException {
        log.debug(String.format(
                "Creating manifest, signatureUri: %s, hashAlgorithm: %s, fileName: %s",
                signatureUri, hashAlgorithm.getName(), fileName));
        final Manifest manifest = new Manifest(signatureUri);
        final Datafile datafile = createDatafile(hashAlgorithm, inputStream, fileName);
        manifest.addDatafile(datafile);
        return manifest;
    }

    /**
     * Create manifest with only signature uri
     *
     * @param signatureUri signature uri in the container
     * @return Manifest
     * @throws TLVParserException
     */
    @Override
    public Manifest createManifest(final String signatureUri) throws TLVParserException {
        log.debug("Creating manifest, signatureUri: " + signatureUri);
        return new Manifest(signatureUri);
    }

    /**
     * Generate new Datafile form input stream
     *
     * @param hashAlgorithm Hash algorithm to be used
     * @param inputStream file input stream
     * @param fileName file name
     * @return Datafile
     * @throws HashException
     * @throws TLVParserException
     */
    @Override
    public Datafile createDatafile(final HashAlgorithm hashAlgorithm, final InputStream inputStream,
                                   final String fileName) throws HashException, TLVParserException {
        log.debug(String.format("Creating datafile, hashAlgorithm: %s, fileName: %s",
                hashAlgorithm.getName(), fileName));
        final Datafile datafile = new Datafile();
        DataHash dataHash = new DataHasher(hashAlgorithm).addData(inputStream).getHash();
        datafile.addElements(fileName, hashAlgorithm, dataHash);
        return datafile;
    }

    /**
     * Generate new Datafile from file
     *
     * @param hashAlgorithm Hash algorithm to be used
     * @param file input file
     * @return Datafile
     * @throws FileNotFoundException
     * @throws TLVParserException
     */
    @Override
    public Datafile createDatafile(final HashAlgorithm hashAlgorithm, final File file)
            throws TLVParserException, FileNotFoundException {
        log.debug(String.format("Creating datafile from file, hashAlgorithm: %s, file: %s",
                hashAlgorithm.getName(), file.getName()));
        InputStream is = new FileInputStream(file);
        return createDatafile(hashAlgorithm, is, file.getName());
    }
}
