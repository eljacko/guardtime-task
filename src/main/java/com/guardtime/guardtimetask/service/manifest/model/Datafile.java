package com.guardtime.guardtimetask.service.manifest.model;

import com.guardtime.guardtimetask.common.exception.ContainerServiceException;
import com.guardtime.ksi.hashing.DataHash;
import com.guardtime.ksi.hashing.HashAlgorithm;
import com.guardtime.ksi.tlv.TLVElement;
import com.guardtime.ksi.tlv.TLVParserException;
import com.guardtime.ksi.tlv.TLVStructure;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Data structure containing metadata of files
 */
@NoArgsConstructor
public class Datafile extends TLVStructure {

    @Getter
    private String uri;
    @Getter
    private HashAlgorithm hashAlgorithm;
    @Getter
    private DataHash hash;

    public static final int DATAFILE_TYPE = 1;
    private static final int DATAFILE_URI_TYPE = 3;
    private static final int DATAFILE_HASH_ALGORITHM_TYPE = 4;
    private static final int DATAFILE_HASH_TYPE = 5;

    public Datafile(final TLVElement root) throws TLVParserException, ContainerServiceException {
        super(root);
        List<TLVElement> children = root.getChildElements();

        for (TLVElement child : children) {
           switch (child.getType()) {
               case DATAFILE_URI_TYPE:
                   this.uri = this.readOnce(child).getDecodedString();
                   break;
               case DATAFILE_HASH_ALGORITHM_TYPE:
                   this.hashAlgorithm = HashAlgorithm
                           .getByName(this.readOnce(child).getDecodedString());
                   break;
               case DATAFILE_HASH_TYPE:
                   this.hash = this.readOnce(child).getDecodedDataHash();
                   break;
               default:
                   throw new ContainerServiceException("Unknown datafile TLVElement type");
           }
        }
    }

    /**
     * Add elements to datafile
     *
     * @param elUri file uri
     * @param elHashAlgorithm hash algorithm used
     * @param elHash file hash
     * @throws TLVParserException
     */
    public void addElements(final String elUri, final HashAlgorithm elHashAlgorithm,
                            final DataHash elHash) throws TLVParserException {
        rootElement = new TLVElement(false, false, DATAFILE_TYPE);
        this.uri = elUri;
        rootElement.addChildElement(TLVElement
                .create(DATAFILE_HASH_ALGORITHM_TYPE, elHashAlgorithm.getName()));
        this.hashAlgorithm = elHashAlgorithm;
        rootElement.addChildElement(TLVElement.create(DATAFILE_HASH_TYPE, elHash));
        this.hash = elHash;
        rootElement.addChildElement(TLVElement.create(DATAFILE_URI_TYPE, elUri));
    }

    /**
     * Get tlv element type
     *
     * @return tlv element type
     */
    @Override
    public int getElementType() {
        return 1;
    }

    /**
     * To string override
     *
     * @return toString
     */
    @Override
    public String toString() {
        return "Datafile=[uri=[" + this.uri + "]," + "hashAlgorithm=["
                + this.hashAlgorithm.getName() + "]," + "hash=[" + this.hash + "]]";
    }
}
