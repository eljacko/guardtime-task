package com.guardtime.guardtimetask.service.manifest.model;

import com.guardtime.guardtimetask.common.exception.ContainerServiceException;
import com.guardtime.ksi.tlv.TLVElement;
import com.guardtime.ksi.tlv.TLVParserException;
import com.guardtime.ksi.tlv.TLVStructure;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Data structure containing metadata signature and datafiles
 */
public class Manifest extends TLVStructure {
    @Getter
    private List<Datafile> datafiles = new ArrayList<>();
    @Getter
    private String signatureUri;

    private static final int MANIFEST_TYPE = 0;
    private static final int DATAFILE_TYPE = 1;
    private static final int SIGNATURE_URI_TYPE = 2;

    public Manifest(final String signatureUri) throws TLVParserException {
        this.rootElement = new TLVElement(false, false, MANIFEST_TYPE);
        this.signatureUri = signatureUri;
        this.rootElement.addChildElement(TLVElement.create(SIGNATURE_URI_TYPE, signatureUri));
    }

    public Manifest(final TLVElement root) throws TLVParserException {
        super(root);

        List<TLVElement> children = root.getChildElements();

        for (TLVElement child : children) {
            switch (child.getType()) {
                case (SIGNATURE_URI_TYPE):
                    this.signatureUri = this.readOnce(child).getDecodedString();
                    break;
                case (DATAFILE_TYPE):
                    Datafile datafile = new Datafile(child);
                    this.datafiles.add(datafile);
                    break;
                default:
                    throw new ContainerServiceException("Unknown manifest TLVElement type");
            }
        }
    }

    /**
     * Adds datafile to manifest
     * @param datafile datafile to be added
     * @throws TLVParserException
     */
    public void addDatafile(final Datafile datafile) throws TLVParserException {
        this.datafiles.add(datafile);
        this.rootElement.addChildElement(datafile.getRootElement());
    }

    /**
     * Get tlv element type
     *
     * @return element type
     */
    @Override
    public int getElementType() {
        return 0;
    }

    /**
     * To string override
     *
     * @return manifest string
     */
    @Override
    public String toString() {
        return "Manifest=[datafiles=[" + this.datafiles + "]," + "signatureUri=["
                + this.signatureUri + "]]";
    }
}
