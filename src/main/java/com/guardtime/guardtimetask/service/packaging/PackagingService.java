package com.guardtime.guardtimetask.service.packaging;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public interface PackagingService {

    void addFile(ZipOutputStream zos, InputStream inputStream, String filename) throws IOException;

    byte[] extractEntry(ZipInputStream zis) throws IOException;

    void transferEntries(ZipInputStream source, ZipOutputStream dest, List<String> entries)
            throws IOException;

}
