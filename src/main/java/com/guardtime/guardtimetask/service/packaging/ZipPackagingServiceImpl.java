package com.guardtime.guardtimetask.service.packaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipPackagingServiceImpl implements PackagingService {
    private static Logger log = LoggerFactory.getLogger(ZipPackagingServiceImpl.class);

    /**
     * A constants for buffer size used to read/write data
     */
    private static final int BUFFER_SIZE = 16384;

    /**
     * Adds a file to the zip output stream
     *
     * @param inputStream inputStream of the file to be added
     * @param name name(uri) of file to be added
     * @param zos the current zip output stream
     * @throws IOException
     */
    @Override
    public void addFile(final ZipOutputStream zos, final InputStream inputStream,
                        final String name) throws IOException {
        log.debug("Adding to zip, file: " + name);
        zos.putNextEntry(new ZipEntry(name));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = inputStream.read(bytesIn)) != -1) {
            zos.write(bytesIn, 0, read);
        }
        zos.closeEntry();
    }

    /**
     * Method for unziping one entry
     *
     * @param zis entry input stream
     * @return FileReference = file data + filename
     * @throws IOException
     */
    @Override
    public byte[] extractEntry(final ZipInputStream zis) throws IOException {
        log.debug("Extracting entry");
        byte[] buffer = new byte[BUFFER_SIZE];
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            int len;
            while ((len = zis.read(buffer)) > 0) {
                output.write(buffer, 0, len);
            }
            return output.toByteArray();
        }
    }

    /**
     * Method for transferring files from old container to modified container
     *
     * @param source inputStream to transfer from
     * @param dest outputStream of zip file to transfer entries from source
     * @param entries files to copy from old container to new container
     * @throws IOException
     */
    @Override
    public void transferEntries(final ZipInputStream source, final ZipOutputStream dest,
                                final List<String> entries) throws IOException {
        log.debug("Transferring entries: " + entries.toString());
        ZipEntry entry;
        while ((entry = source.getNextEntry()) != null) {
            if (entries.contains(entry.getName())) {
                dest.putNextEntry(new ZipEntry(entry));
                copyStream(source, dest, new byte[BUFFER_SIZE]);
                dest.closeEntry();
            }
        }
    }

    private static long copyStream(final InputStream source, final OutputStream dest,
                                   final byte[] buffer) throws IOException {
        long read;
        int n;
        n = source.read(buffer);
        for (read = 0L; -1 != n; read += n) {
            dest.write(buffer, 0, n);
            n = source.read(buffer);
        }
        return read;
    }
}
