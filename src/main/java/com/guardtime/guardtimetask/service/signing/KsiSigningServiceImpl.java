package com.guardtime.guardtimetask.service.signing;

import com.guardtime.ksi.Signer;
import com.guardtime.ksi.SignerBuilder;
import com.guardtime.ksi.exceptions.KSIException;
import com.guardtime.ksi.hashing.DataHash;
import com.guardtime.ksi.service.KSISigningClientServiceAdapter;
import com.guardtime.ksi.service.client.KSIServiceCredentials;
import com.guardtime.ksi.service.client.KSISigningClient;
import com.guardtime.ksi.service.client.ServiceCredentials;
import com.guardtime.ksi.service.client.http.CredentialsAwareHttpSettings;
import com.guardtime.ksi.service.http.simple.SimpleHttpSigningClient;
import com.guardtime.ksi.unisignature.KSISignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * KSI signing service implementation
 *
 * Missing from unit tests as is implementation of external services
 * and functionality is covered with container service tests
 */
public class KsiSigningServiceImpl implements SigningService {
    private static Logger log = LoggerFactory.getLogger(SigningService.class);

    private final String aggregatorUrl;
    private final String loginId;
    private final String loginKey;

    private KSISigningClient ksiSigningClient;
    private Signer signer;

    public KsiSigningServiceImpl(final String aggregatorUrl, final String loginId,
                                 final String loginKey) {
        this.aggregatorUrl = aggregatorUrl;
        this.loginId = loginId;
        this.loginKey = loginKey;
    }

    /**
     * Sets up KSI signing service
     *
     * @throws KSIException
     */
    @Override
    public void setUpKsi() throws KSIException {
        log.debug("Setting up Ksi");
        ServiceCredentials credentials = new KSIServiceCredentials(loginId, loginKey);
        ksiSigningClient = new SimpleHttpSigningClient(
                new CredentialsAwareHttpSettings(aggregatorUrl, credentials));
        signer = new SignerBuilder()
                .setSigningService(new KSISigningClientServiceAdapter(ksiSigningClient)).build();
    }

    /**
     * Closes signing service
     */
    @Override
    public void tearDownKsi() {
        log.debug("Tearing down Ksi");
        try {
            if (signer != null) {
                signer.close();
            }
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    /**
     * Signs DataHash
     *
     * @param dataHash has to be signed
     * @return KSI signature file
     * @throws KSIException
     */
    @Override
    public KSISignature sign(final DataHash dataHash) throws KSIException {
        log.debug("Signing DataHash: " + dataHash);
        return signer.sign(dataHash);
    }

    /**
     * Signs provided document bites
     *
     * @param document document to be signed
     * @return KSI signature file
     * @throws KSIException
     */
    @Override
    public KSISignature sign(final byte[] document) throws KSIException {
        log.debug("Signing document");
        return signer.sign(document);
    }
}
