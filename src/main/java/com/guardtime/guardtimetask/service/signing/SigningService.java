package com.guardtime.guardtimetask.service.signing;


import com.guardtime.ksi.exceptions.KSIException;
import com.guardtime.ksi.hashing.DataHash;
import com.guardtime.ksi.unisignature.KSISignature;

public interface SigningService {

    KSISignature sign(DataHash dataHash) throws KSIException;

    KSISignature sign(byte[] document) throws KSIException;

    void setUpKsi() throws KSIException;

    void tearDownKsi();
}
