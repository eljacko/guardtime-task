package com.guardtime.guardtimetask.service.container;

import com.guardtime.guardtimetask.common.exception.ContainerServiceException;
import com.guardtime.guardtimetask.service.ZipContainerServiceImpl;
import com.guardtime.guardtimetask.utils.UnitTestHelper;
import com.guardtime.ksi.hashing.HashAlgorithm;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings({ "checkstyle:MethodName", "checkstyle:magicnumber",
        "checkstyle:VisibilityModifier" })
public class ContainerServiceAddFileTests {
    private static ZipContainerServiceImpl containerService;

    @TempDir
    File tempDir = new File("container_temp_dir");

    @BeforeAll
    public static void init() {
        String aggregatorUrl = System.getProperty("ksi.aggregator.url");
        String loginId = System.getProperty("ksi.login.id");
        String loginKey = System.getProperty("ksi.login.key");
        containerService = new ZipContainerServiceImpl(aggregatorUrl, loginId, loginKey);
    }

    /**
     * Close the container just in case between tests
     */
    @AfterEach
    public void cleanUpEach() {
        try {
            containerService.closeContainer();
        } catch (Exception ignored) { }
    }

    /**
     * test adding single file
     *
     * @param size file size from ValueSource
     * @throws IOException
     */
    @ParameterizedTest
    @ValueSource(ints = {1024, 1024 * 1024, 1024 * 1024 * 1024})
    public void addFile_addSingleFile_containerContainsFile(final int size) throws IOException {
        final String containerName = "temp_container.zip";
        File container = new File(tempDir, containerName);

        File datafile = UnitTestHelper.getTestFile(size);

        containerService.createContainer(container);
        containerService.addFile(datafile, HashAlgorithm.SHA2_256);

        assertAll(
                () -> assertEquals(1, containerService.getDatafiles().size()),
                () -> assertEquals(0, containerService.getAddedMetadata().size())
        );

        containerService.closeContainer();
        List<String> filesInContainer =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertAll(
                () -> assertTrue(Files.exists(container.toPath())),
                () -> assertTrue(filesInContainer.contains(datafile.getName()))
        );
    }

    /**
     * test adding multiple files
     *
     * @param size file size from ValueSource
     * @throws IOException
     */
    @ParameterizedTest
    @ValueSource(ints = {1024, 1024 * 1024, 1024 * 1024 * 1024})
    public void addFile_addMultipleFiles_containerContainsAllFiles(final int size)
            throws IOException {
        final String containerName = "temp_container.zip";
        File container = new File(tempDir, containerName);

        File datafile1 = UnitTestHelper.getTestFile(size);
        File datafile2 = UnitTestHelper.getTestFile(size);
        File datafile3 = UnitTestHelper.getTestFile(size);

        containerService.createContainer(container);
        containerService.addFile(datafile1, HashAlgorithm.SHA2_256);
        containerService.addFile(datafile2, HashAlgorithm.SHA2_256);
        containerService.addFile(datafile3, HashAlgorithm.SHA2_256);

        assertAll(
                () -> assertEquals(3, containerService.getDatafiles().size()),
                () -> assertEquals(0, containerService.getAddedMetadata().size())
        );

        containerService.closeContainer();
        List<String> filesInContainer =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertAll(
                () -> assertTrue(Files.exists(container.toPath())),
                () -> assertTrue(filesInContainer.contains(datafile1.getName())),
                () -> assertTrue(filesInContainer.contains(datafile2.getName())),
                () -> assertTrue(filesInContainer.contains(datafile3.getName()))
        );
    }

    /**
     * test adding multiple files
     *
     * @param numberOfFiles amount of files to be added to container
     * @throws IOException
     */
    @ParameterizedTest
    @ValueSource(ints = {100, 1000})
    public void addFile_addLoadsOfFiles_containerContainsAllFiles(final int numberOfFiles)
            throws IOException {
        File container = new File(tempDir, "temp_container.zip");
        List<File> files = new ArrayList<>();
        for (int i = 0; i < numberOfFiles; i++) {
            files.add(UnitTestHelper.getTestFile(1024));
        }

        containerService.createContainer(container);
        for (File file : files) {
            containerService.addFile(file, HashAlgorithm.SHA2_256);
        }
        containerService.closeContainer();
        List<String> filesInContainer =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertEquals(numberOfFiles, filesInContainer.size());
    }

    @Test
    public void addFile_containerNotOpen_throwsError() throws IOException {
        File datafile = UnitTestHelper.getTestFile(1024);

        assertThrows(ContainerServiceException.class, () -> {
            containerService.addFile(datafile, HashAlgorithm.SHA2_256);
        });
    }



    @Test
    public void addFiles_addsAllFiles_hasSameAmountOfFiles() throws IOException {
        File container = new File(tempDir, "temp_container.zip");
        final int numberOfFiles = 5;
        List<File> files = new ArrayList<>();
        for (int i = 0; i < numberOfFiles; i++) {
            files.add(UnitTestHelper.getTestFile(1024));
        }

        containerService.createContainer(container);
        containerService.addFiles(files, HashAlgorithm.SHA2_256);
        containerService.closeContainer();
        List<String> filesInContainer =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertEquals(numberOfFiles, filesInContainer.size());
    }

    @Test
    public void addFiles_containerNotOpen_throwsError() {
        List<File> files = new ArrayList<>();

        assertThrows(ContainerServiceException.class, () -> {
            containerService.addFiles(files, HashAlgorithm.SHA2_256);
        });
    }
}
