package com.guardtime.guardtimetask.service.container;

import com.guardtime.guardtimetask.common.exception.ContainerServiceException;
import com.guardtime.guardtimetask.service.ZipContainerServiceImpl;
import com.guardtime.guardtimetask.utils.UnitTestHelper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SuppressWarnings({ "checkstyle:MethodName", "checkstyle:VisibilityModifier" })
public class ContainerServiceCreateOpenAndCloseTests {
    private static ZipContainerServiceImpl containerService;

    @TempDir
    File tempDir = new File("container_temp_dir");

    @BeforeAll
    public static void init() {
        String aggregatorUrl = System.getProperty("ksi.aggregator.url");
        String loginId = System.getProperty("ksi.login.id");
        String loginKey = System.getProperty("ksi.login.key");
        containerService = new ZipContainerServiceImpl(aggregatorUrl, loginId, loginKey);
    }

    /**
     * Close the container just in case between tests
     */
    @AfterEach
    public void cleanUpEach() {
        try {
            containerService.closeContainer();
        } catch (Exception ignored) { }
    }

    /**
     * Error thrown from closeContainer service when container is not initiated
     */
    @Test
    public void createContainer_callOnce_noError() {
        File container = new File(tempDir, "temp_container.zip");

        containerService.createContainer(container);
        assertDoesNotThrow(() -> {
            containerService.closeContainer();
        });

    }

    @Test
    public void createContainer_callTwice_exceptionThrown() {
        File container = new File(tempDir, "temp_container.zip");
        containerService.createContainer(container);
        assertThrows(ContainerServiceException.class, () -> {
            containerService.createContainer(container);
        });
    }

    @Test
    public void openContainer_open_loadsData() throws URISyntaxException {
        File container = UnitTestHelper.loadFileFromClasspath("container_one_file_unsigned.zip");
        containerService.openContainer(container);
        assertEquals(1, containerService.getLoadedFiles().size());
        assertDoesNotThrow(() -> {
            containerService.closeContainer();
        });
    }

    @Test
    public void openContainer_callTwice_exceptionThrown() throws URISyntaxException {
        File container = UnitTestHelper.loadFileFromClasspath("container_one_file_unsigned.zip");
        containerService.openContainer(container);
        assertThrows(ContainerServiceException.class, () -> {
            containerService.openContainer(container);
        });
    }

}
