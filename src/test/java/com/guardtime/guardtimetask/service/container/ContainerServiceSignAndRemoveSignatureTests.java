package com.guardtime.guardtimetask.service.container;

import com.guardtime.guardtimetask.service.ZipContainerServiceImpl;
import com.guardtime.guardtimetask.utils.UnitTestHelper;
import com.guardtime.ksi.exceptions.KSIException;
import com.guardtime.ksi.hashing.HashAlgorithm;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SuppressWarnings({ "checkstyle:MethodName", "checkstyle:magicnumber",
        "checkstyle:VisibilityModifier"  })
public class ContainerServiceSignAndRemoveSignatureTests extends ContainerServiceTests {

    private static ZipContainerServiceImpl containerService;

    @TempDir
    File tempDir = new File("container_temp_dir");
    File resourcesDirectory = new File("src/test/resources");

    @BeforeAll
    public static void init() {
        String aggregatorUrl = System.getProperty("ksi.aggregator.url");
        String loginId = System.getProperty("ksi.login.id");
        String loginKey = System.getProperty("ksi.login.key");
        containerService = new ZipContainerServiceImpl(aggregatorUrl, loginId, loginKey);
    }

    /**
     * Close the container just in case between tests
     */
    @AfterEach
    public void cleanUpEach() {
        try {
            containerService.closeContainer();
        } catch (Exception ignored) { }
    }

    @Test
    public void addSignature_newContainer_metaInfoCreated() throws IOException {
        final String containerName = "temp_container.zip";
        File container = new File(tempDir, containerName);
        File datafile = UnitTestHelper.getTestFile(1024);

        containerService.createContainer(container);
        containerService.addFile(datafile, HashAlgorithm.SHA2_256);
        containerService.addSignature();

        assertAll(
                () -> assertEquals(1, containerService.getDatafiles().size()),
                () -> assertEquals(2, containerService.getAddedMetadata().size())
        );

        containerService.closeContainer();

        List<String> filesInContainer =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertAll(
                () -> assertTrue(Files.exists(container.toPath())),
                () -> assertTrue(filesInContainer.contains(datafile.getName())),
                () -> assertTrue(filesInContainer.contains("/META-INF/signature1.ksi")),
                () -> assertTrue(filesInContainer.contains("/META-INF/manifest1.tlv")),
                () -> assertEquals(3, filesInContainer.size())
        );
    }

    @Test
    public void addSignature_existingContainer_MetadataAdded() throws IOException, KSIException {
        File container = getContainer(tempDir, containerService, 1, 0);
        containerService.openContainer(container);

        assertEquals(1,
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container)).size());

        containerService.addSignature();
        assertAll(
                () -> assertEquals(1, containerService.getLoadedFiles().size()),
                () -> assertEquals(2, containerService.getAddedMetadata().size())
        );

        containerService.closeContainer();

        List<String> filesInContainer =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertAll(
                () -> assertTrue(Files.exists(container.toPath())),
                () -> assertTrue(filesInContainer.contains("/META-INF/signature1.ksi")),
                () -> assertTrue(filesInContainer.contains("/META-INF/manifest1.tlv")),
                () -> assertEquals(3, filesInContainer.size())
        );
    }

    @Test
    public void removeSignature_openContainerWithOneSignature_noSignatures()
            throws IOException, KSIException {
        File container = getContainer(tempDir, containerService, 1, 1);
        List<String> filesInContainerBefore =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertAll(
                () -> assertTrue(filesInContainerBefore.contains("/META-INF/signature1.ksi")),
                () -> assertTrue(filesInContainerBefore.contains("/META-INF/manifest1.tlv")),
                () -> assertEquals(3, filesInContainerBefore.size())
        );

        containerService.openContainer(container);
        containerService.removeSignature("/META-INF/signature1.ksi");
        containerService.closeContainer();

        List<String> filesInContainerAfter =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertAll(
                () -> assertFalse(filesInContainerAfter.contains("/META-INF/signature1.ksi")),
                () -> assertFalse(filesInContainerAfter.contains("/META-INF/manifest1.tlv")),
                () -> assertEquals(1, filesInContainerAfter.size())
        );
    }

    @Test
    public void removeSignature_createContainerSignAndRemove_noSignatureSaved() throws IOException {
        final String containerName = "temp_container.zip";
        File container = new File(resourcesDirectory, containerName);
        File datafile = UnitTestHelper.getTestFile(1024);
        containerService.createContainer(container);
        containerService.addFile(datafile, HashAlgorithm.SHA2_256);
        containerService.addSignature();
        containerService.removeSignature("/META-INF/signature1.ksi");
        containerService.closeContainer();

        List<String> filesInContainer =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));
        assertAll(
                () -> assertTrue(filesInContainer.contains(datafile.getName())),
                () -> assertFalse(filesInContainer.contains("/META-INF/signature1.ksi")),
                () -> assertFalse(filesInContainer.contains("/META-INF/manifest1.tlv")),
                () -> assertEquals(1, filesInContainer.size())
        );
        assertTrue(filesInContainer.contains(datafile.getName()));
    }

    @Test
    public void removeSignature_openContainerWithThreeSignaturesRemoveOne_twoSignaturesLeft()
            throws IOException, KSIException {
        File container = getContainer(tempDir, containerService, 3, 3);
        List<String> filesInContainerBefore =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertAll(
                () -> assertTrue(filesInContainerBefore.contains("/META-INF/signature1.ksi")),
                () -> assertTrue(filesInContainerBefore.contains("/META-INF/manifest1.tlv")),
                () -> assertTrue(filesInContainerBefore.contains("/META-INF/signature2.ksi")),
                () -> assertTrue(filesInContainerBefore.contains("/META-INF/manifest2.tlv")),
                () -> assertTrue(filesInContainerBefore.contains("/META-INF/signature3.ksi")),
                () -> assertTrue(filesInContainerBefore.contains("/META-INF/manifest3.tlv")),
                () -> assertEquals(9, filesInContainerBefore.size())
        );

        containerService.openContainer(container);
        containerService.removeSignature("/META-INF/signature2.ksi");
        containerService.closeContainer();

        List<String> filesInContainerAfter =
                UnitTestHelper.getFileNamesInContainer(new FileInputStream(container));

        assertAll(
                () -> assertTrue(filesInContainerAfter.contains("/META-INF/signature1.ksi")),
                () -> assertTrue(filesInContainerAfter.contains("/META-INF/manifest1.tlv")),
                () -> assertFalse(filesInContainerAfter.contains("/META-INF/signature2.ksi")),
                () -> assertFalse(filesInContainerAfter.contains("/META-INF/manifest2.tlv")),
                () -> assertTrue(filesInContainerAfter.contains("/META-INF/signature3.ksi")),
                () -> assertTrue(filesInContainerAfter.contains("/META-INF/manifest3.tlv")),
                () -> assertEquals(7, filesInContainerAfter.size())
        );
    }

}
