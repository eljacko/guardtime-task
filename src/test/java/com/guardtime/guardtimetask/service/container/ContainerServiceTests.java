package com.guardtime.guardtimetask.service.container;

import com.guardtime.guardtimetask.service.ContainerService;
import com.guardtime.guardtimetask.utils.UnitTestHelper;
import com.guardtime.ksi.exceptions.KSIException;
import com.guardtime.ksi.hashing.HashAlgorithm;

import java.io.File;
import java.io.IOException;

public class ContainerServiceTests {

    /**
     * Helper method to build containers for container service test
     * @param dir
     * @param service
     * @param files
     * @param signatures
     * @return
     * @throws IOException
     * @throws KSIException
     */
    File getContainer(final File dir, final ContainerService service,
                      final int files, final int signatures) throws IOException, KSIException {
        final String containerName = "temp_container.zip";
        final int size = 1024;
        File container = new File(dir, containerName);
        service.createContainer(container);
        int i = 0;
        while (i < files || i < signatures) {
            if (i < files) {
                File datafile = UnitTestHelper.getTestFile(size);
                service.addFile(datafile, HashAlgorithm.SHA2_256);
            }
            if (i < signatures) {
                service.addSignature();
            }
            i++;
        }
        service.closeContainer();
        return container;
    }

}
