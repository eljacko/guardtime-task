package com.guardtime.guardtimetask.service.manifest;

import com.guardtime.guardtimetask.service.manifest.model.Datafile;
import com.guardtime.guardtimetask.service.manifest.model.Manifest;
import com.guardtime.guardtimetask.utils.UnitTestHelper;
import com.guardtime.ksi.hashing.HashAlgorithm;
import com.guardtime.ksi.tlv.TLVParserException;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URISyntaxException;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings({ "checkstyle:MethodName" })
public class ManifestServiceTest {

    private final ManifestService manifestService = new ManifestServiceImpl();

    @Test
    public final void createManifest_OnlySignatureUri_ReturnManifest() throws TLVParserException {
        final String signatureUri = "/META-INF/signature1.ksi";

        Manifest manifest = manifestService.createManifest(signatureUri);

        assertEquals(signatureUri, manifest.getSignatureUri());
    }

    @Test
    public final void createManifest_AllArguments_ReturnManifestWithOneDatafile()
            throws TLVParserException {
        final String signatureUri = "/META-INF/signature1.ksi";
        final String file = "datafile1.pdf";
        final HashAlgorithm algorithm = HashAlgorithm.SHA2_512;
        final String hash = "SHA-512:[677BDBBB414DD493DD217938541779E1D6FBF66CF0850A122585105524B"
                + "0BC5C9A63F3671F22A59C6CC18E97050B10F8096F92D2CDCCC3966638D44995A03EBC]";

        InputStream is = this.getClass().getClassLoader().getResourceAsStream(file);

        Manifest manifest = manifestService.createManifest(signatureUri, algorithm, is, file);

        System.out.println(manifest.toString());
        assertAll(
                () -> assertEquals(hash, manifest.getDatafiles().get(0).getHash().toString()),
                () -> assertEquals(1, manifest.getDatafiles().size()),
                () -> assertEquals(signatureUri, manifest.getSignatureUri())
        );
    }

    @Test
    public final void createDatafile_FromFileSHA2_512_ReturnDatafile() throws URISyntaxException,
            FileNotFoundException, TLVParserException {
        final String fileName = "datafile1.pdf";
        final String hash = "SHA-512:[677BDBBB414DD493DD217938541779E1D6FBF66CF0850A122585105524B"
                + "0BC5C9A63F3671F22A59C6CC18E97050B10F8096F92D2CDCCC3966638D44995A03EBC]";
        final HashAlgorithm algorithm = HashAlgorithm.SHA2_512;
        File file = UnitTestHelper.loadFileFromClasspath(fileName);

        Datafile datafile = manifestService.createDatafile(algorithm, file);

        assertEquals(fileName, datafile.getUri());
        assertEquals(algorithm.getName(), datafile.getHashAlgorithm().getName());
        assertEquals(hash, datafile.getHash().toString());
    }

    @Test
    public final void createDatafile_FromInputStreamSHA2_256_ReturnDatafile()
            throws TLVParserException {
        final String file = "datafile1.pdf";
        final String hash =
                "SHA-256:[5A0EEA13FB5E7AF38CF072E252B4B72EE9785332A1659A6E6D235F884A8394C9]";
        final HashAlgorithm algorithm = HashAlgorithm.SHA2_256;

        InputStream is = this.getClass().getClassLoader().getResourceAsStream(file);

        Datafile datafile = manifestService.createDatafile(algorithm, is, file);

        assertEquals(file, datafile.getUri());
        assertEquals(algorithm.getName(), datafile.getHashAlgorithm().getName());
        assertEquals(hash, datafile.getHash().toString());
    }
}
