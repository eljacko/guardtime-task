package com.guardtime.guardtimetask.service.packaging;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipOutputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SuppressWarnings({ "checkstyle:MethodName" })
public class PackagingServiceTest {

    private final PackagingService packagingService = new ZipPackagingServiceImpl();

    @Test
    public void addFile_addSingleFile_OutputHasLength() throws IOException {
        final String file = "datafile2.txt";
        final Integer expectedLength = 87;

        InputStream in = this.getClass().getClassLoader().getResourceAsStream(file);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipOutputStream zipOut = new ZipOutputStream(out);

        packagingService.addFile(zipOut, in, file);

        byte[] zipFile = out.toByteArray();
        assertEquals(expectedLength, zipFile.length);
    }

    @Test
    public void addFile_twoSingleFiles_OutputHasLength() throws IOException {
        final String file1 = "datafile1.pdf";
        final String file2 = "datafile2.txt";
        final Integer expectedLength = 13216;

        InputStream in1 = this.getClass().getClassLoader().getResourceAsStream(file1);
        InputStream in2 = this.getClass().getClassLoader().getResourceAsStream(file2);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipOutputStream zipOut = new ZipOutputStream(out);

        packagingService.addFile(zipOut, in1, file1);
        packagingService.addFile(zipOut, in2, file2);

        byte[] zipFile = out.toByteArray();
        assertEquals(expectedLength, zipFile.length);
    }

}
