package com.guardtime.guardtimetask.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Helper methods for unit tests
 */
public final class UnitTestHelper {
    private UnitTestHelper() {
        throw new java.lang.UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }

    public static File getTestFile(final Integer size) throws IOException {
        final File file = File.createTempFile("testFile_", ".tmp");
        RandomAccessFile tmpFile = new RandomAccessFile(file, "rw");
        tmpFile.setLength(size);
        tmpFile.close();
        return file;
    }

    public static List<String> getFileNamesInContainer(final InputStream is) throws IOException {
        ZipInputStream zis = new ZipInputStream(is);

        ZipEntry entry;

        List<String> files = new ArrayList<>();
        while ((entry = zis.getNextEntry()) != null) {
            files.add(entry.getName());
        }
        return files;
    }

    public static File loadFileFromClasspath(final String file) throws URISyntaxException {
        URL resource = UnitTestHelper.class.getClassLoader().getResource(file);
        if (resource == null) {
            throw new IllegalArgumentException("file not found!");
        } else {
            return new File(resource.toURI());
        }
    }

}
